# mocha

```yaml
# 🚧 WIP
stages:
  - 🧨unit-tests

include:
  - project: 'simplify-devops/mocha'
    file: 'mocha.gitlab-ci.yml'

#-----------------------------------------------------------------------------------------
# Tests report in the MR
#-----------------------------------------------------------------------------------------
🎯testing:
  stage: 🧨unit-tests
  extends: .mocha:testing
  rules:
    - if: $CI_MERGE_REQUEST_IID
```
