# mocha helper 2020-09-07 by @k33g - https://gitlab.bots.garden
FROM node:13.12-slim

RUN npm install -g mocha
RUN npm install -g mocha-junit-reporter
